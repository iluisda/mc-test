import template from './edit-user.html';
import controller from './edit-user.controller';

export default {
	template: template,
	controller: controller
};
