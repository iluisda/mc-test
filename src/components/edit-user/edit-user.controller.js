export default class EditUserController {
	constructor($log, $scope, userService, $state) {
		'ngInject';

		this.$log = $log;
		this.$scope = $scope;
		this.userService = userService;
		this.$state = $state;

	}

	$onInit = () => {
    this.users = this.userService.dataStore;
		this.$log.info('Activated Edit User View.');
    this.restoreData = this.$state.params.data;
    this.user = (this.$state.params.data) ? this.$state.params.data : {};
    if (!this.restoreData) {
      this.title = 'Add user';
    } else {
      this.title = 'Edit user';
    }
	};
  submit(data) {
    if (!this.restoreData) {
      const user = {
        id: this.users.length + 1,
        name: data.name ? data['name'] : '',
        username: data.username ? data['username'] : '',
        email: data.email ? data['email'] : '',
        website: data.website ? data['website'] : '',
        company: data.company ? data['website'] : '',
        category: data.category
      };
  
      this.userService.dataStore.push(user);
      
    } else {
      let beta = this.users.filter((user) => user.id === data.id)[0];
      beta = data;
    }
    
    this.$state.go('user');
  }
  convertString = (num) => {
    return num.toString();
  }
}
