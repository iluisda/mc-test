import editUserComponent from './edit-user.component';

const editUserModule = angular.module('app.edit-user', []);

// loading components, services, directives, specific to this module.
editUserModule.component('edit', editUserComponent);

// export this module
export default editUserModule;
