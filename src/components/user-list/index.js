import userListComponent from './user-list.component';

const userListModule = angular.module('app.list', []);
userListModule.component('list', userListComponent);

export default userListModule;
