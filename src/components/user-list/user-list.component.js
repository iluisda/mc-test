import template from './user-list.html';
import controller from './user-list.controller';

const userListComponent = {
  controller,
  template: template,
  bindings: {
    data: '<'
  }
};

export default userListComponent;
