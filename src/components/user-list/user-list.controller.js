class userListController {
	constructor(userService, $state, $timeout) {
		'ngInject'
		this.userService = userService;
		this.$state = $state;
		this.$timeout = $timeout;
	}
	$onInit = () => {
		this.$timeout(() => {
			this.users = this.data;
			
		}, 400);
	};

	actionDelete(array, id, element) {
		for (let i = 0; i < array.length; i++) {
			if (array[i] === array[element]) {
				array.splice(i, 1);
				this.userService.updateUsers(array);
			}
		}
	};
	userEdit(data) {
    this.$state.go('edit', {data: data})
  };
}

export default userListController;
