export default class UserController {
	constructor(
		$log,
		userService,
		$scope,
		$timeout
	) {
		'ngInject';

		this.$log = $log;
		this.$scope = $scope;
		this.$timeout = $timeout;
		this.userService = userService;
	}

	$onInit = () => {
		this.users = new Array();
		if (this.userService.dataStore.length === 0) {
			this.userService.get().then((users) => {
				angular.forEach(users, (result, index) => {
					const user = {
						id: result.id,
						name: result.name,
						username: result.username,
						email: result.email,
						website: result.website,
						company: result.company.name
					};

					user.category = this.setUserCategory(user, index);
					this.users.push(user);
					this.userService.saveUser(user);

				});
				this.busqueda = this.users;
			});

		} else {
			this.users = this.userService.dataStore;
			this.busqueda = this.users;
		}

		this.$log.info('Activated User View.');
	};

	setUserCategory(user, index) {
		if (index % 2 !== 0) {
			return "admin"
		}
		return "user"
	};

}
