export default [
	// {
	// 	name: 'home',
	// 	url: '/',
	// 	component: 'home'
	// },
	{
		name: 'user',
		url: '/',
		component: 'user'
	},
	{
		name: 'edit',
		url: '/edit-user',
		component: 'edit',
		params: {
			data: null
		}
	}
];
