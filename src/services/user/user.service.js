export default class UserService {
	constructor(
		$http
	) {
		'ngInject';

		this.$http = $http;
		this.dataStore = new Array();
	}

	get = () => {
		return this.$http.get('https://jsonplaceholder.typicode.com/users')
			.then((response) => {
				return response.data;
			});
	};

	saveUser = (data) => {
		this.dataStore.push(data);
	};

	updateUsers = (data) => {
		this.dataStore = new Array();
		this.dataStore = data;
	};
}
